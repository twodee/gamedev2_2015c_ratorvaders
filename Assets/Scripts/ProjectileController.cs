﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {
  public float speed;
  private new Rigidbody2D rigidbody;

	void Start() {
    rigidbody = GetComponent<Rigidbody2D>();	
    rigidbody.velocity = Vector3.up * speed;
	}
	
  void OnTriggerEnter2D(Collider2D other) {
    if (other.gameObject.tag == "Black Hole") {
      print("trigger enter");
      StartCoroutine(DieSoon());
    }
  }

  IEnumerator DieSoon() {
    rigidbody.angularVelocity = 100;
    yield return new WaitForSeconds(5);
    Destroy(gameObject);
  }
}
