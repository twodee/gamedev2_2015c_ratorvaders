﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
  public float speed;

  private new Rigidbody2D rigidbody;

	void Start() {
    rigidbody = GetComponent<Rigidbody2D>();	
    rigidbody.velocity = Vector3.up * -speed;
	}

  void OnTriggerEnter2D(Collider2D other) {
    Destroy(other.gameObject);
    Destroy(gameObject);
  }
}
