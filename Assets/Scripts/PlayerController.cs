﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
  public float speed;
  public Sprite idleSprite;
  public Sprite leftSprite;
  public Sprite rightSprite;
  public GameObject projectilePrefab;

  private GameObject projectiles;
  private new Rigidbody2D rigidbody;
  private float timeOfLastFire = 0;
  private float timeBetweenFires = 1;

  void Start() {
    rigidbody = GetComponent<Rigidbody2D>();
    projectiles = GameObject.Find("/Projectiles");
  }

  void Update () {
    float push = Input.GetAxis("Horizontal"); 
    rigidbody.velocity = new Vector3(push * speed, 0, 0);

    if (push < 0) {
      GetComponent<SpriteRenderer>().sprite = leftSprite;
    } else if (push > 0) {
      GetComponent<SpriteRenderer>().sprite = rightSprite;
    } else {
      GetComponent<SpriteRenderer>().sprite = idleSprite;
    }

    if (timeOfLastFire + timeBetweenFires <= Time.time && Input.GetButton("Jump")) {
      timeOfLastFire = Time.time;
      GameObject projectile = (GameObject) Instantiate(projectilePrefab, transform.position, Quaternion.identity);
      projectile.transform.parent = projectiles.transform;
    }

    if (Input.GetMouseButtonUp(0)) {
      Vector3 mousePosition = Input.mousePosition;
      Vector3 destinationWorld = Camera.main.ScreenToWorldPoint(mousePosition);
      destinationWorld.z = 0.0f;
      destinationWorld.y = transform.position.y;
      print(mousePosition + " -> " + destinationWorld);
      /* transform.position = destinationWorld; */
      StartCoroutine(MoveTo(destinationWorld));
    }
  }

  IEnumerator MoveTo(Vector3 endPosition) {
    Vector3 startPosition = transform.position;
    float speed = 12.0f;
    float time = Vector3.Distance(startPosition, endPosition) / speed;
    float startedAt = Time.time;
    while (Time.time - startedAt < time) {
      float elapsed = Time.time - startedAt;
      transform.position = Vector3.Lerp(startPosition, endPosition, elapsed / time);
      yield return null;
    }
  }
}
